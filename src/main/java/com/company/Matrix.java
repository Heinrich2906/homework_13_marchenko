package com.company;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.*;

/**
 * Created by heinr on 09.11.2016.
 */
public class Matrix {

    int[][] matrix_1;
    int[][] matrix_2;

    Matrix(int height) {

        matrix_1 = new int[height][height];
        matrix_2 = new int[height][height];

        //Заполнение исходных матриц случайными значениями
        if (height > 0) {

            Random rand = new Random();

            for (int i = 0; i < height; i++) {
                for (int j = 0; j < height; j++) {
                    this.matrix_1[i][j] = rand.nextInt(1939);
                    this.matrix_2[i][j] = rand.nextInt(1939);
                }
            }
        }
    }

    public static void main(String[] args) throws InterruptedException, IOException {

        int taskCount = 4;
        int height = 25;

        Matrix matrix = new Matrix(height);

        int[][] resultMatrix = new int[height][height];

        List<Callable<int[][]>> tasks = createTasks(taskCount, height, matrix);

        ExecutorService executorService1Thread = Executors.newFixedThreadPool(4);
        long start = System.currentTimeMillis();
        List<Future<int[][]>> futures = executorService1Thread.invokeAll(tasks);
        resultMatrix = getResult(futures, height);
        executorService1Thread.shutdown();
        long time = System.currentTimeMillis() - start;
        System.out.println("Parallel calculation (1) took: " + time + " milliseconds.");
        writeToFile(resultMatrix, "1.txt");
    }

    public static List<Callable<int[][]>> createTasks(int tasksCount, int height, Matrix matrix) {

        List<Callable<int[][]>> tasks = new ArrayList<>();

        int partSize = height / tasksCount;
        int finishLine = 0;
        int startLine = 0;

        for (int i = 0; i < tasksCount; i++) {

            if (i == 0) startLine = 0;
            else startLine = (i - 1) * partSize + 1;

            if (i == tasksCount - 1) finishLine = height;
            else finishLine = i * partSize + 1;

            final int finalFinishLine = finishLine;
            final int finalStartLine = startLine;
            final int finalHeight = height;

            Callable<int[][]> callable = new Callable<int[][]>() {
                @Override
                public int[][] call() throws Exception {
                    return matrix.part(matrix.matrix_1, matrix.matrix_2, finalHeight, finalStartLine, finalFinishLine);
                }
            };

            tasks.add(callable);
        }
        return tasks;
    }

    public int[][] part(int[][] matrix_1, int[][] matrix_2, int height, int startLine, int finishLine) {

        if (startLine < 0 | finishLine < 0) {
            System.out.println("Start (finish) line is less then 0");
            return new int[0][0];
        }
        if (startLine > height | finishLine > height) {
            System.out.println("Start (finish) line is more then " + height);
            return new int[0][0];
        }

        if (startLine > finishLine) {
            System.out.println("Start line is more then Finish line");
            return new int[0][0];
        }

        int[][] matrix_result = new int[height][height];

        for (int i = startLine; i < finishLine; i++) {

            for (int j = 0; j < height; j++) {

                matrix_result[i][j] = 0;

                for (int k = 0; k < height; k++) {
                    matrix_result[i][j] = matrix_result[i][j] + matrix_1[i][k] * matrix_2[k][j];
                }
            }
        }
        return matrix_result;
    }

    public static int[][] getResult(List<Future<int[][]>> futures, int height) {
        try {
            int[][] result = new int[height][height];

            for (int k = 0; k < futures.size(); k++) {
                Future<int[][]> f = futures.get(k);
                int[][] part = f.get();

                for (int i = 0; i < height; i++) {
                    for (int j = 0; j < height; j++) {
                        result[i][j] = result[i][j] + part[i][j];
                    }
                }
            }
            return result;
        } catch (ExecutionException | InterruptedException e) {
            e.printStackTrace();
            return null;
        }
    }

    private static void writeToFile(int[][] matrix, String fileName) throws IOException {
        BufferedWriter fileWriter = new BufferedWriter(new FileWriter(fileName));
        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix[0].length; j++) {
                fileWriter.write(matrix[i][j] + " ");
            }
            fileWriter.newLine();
        }
        fileWriter.close();
    }
}
